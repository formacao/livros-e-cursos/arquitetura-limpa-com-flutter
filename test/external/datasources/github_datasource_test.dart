import 'package:clean_arch/domain/errors/http_status_code_exception.dart';
import 'package:clean_arch/external/datasources/github_datasource.dart';
import 'package:flutter_test/flutter_test.dart';
import '../fixtures/http_client_fixture.dart';
import '../fixtures/http_client_throws_exception_fixture.dart';

void main()
{
  test("Get Search deve retornar uma lista", getSearchDeveRetornarUmaListDeResultSearchResponselModel);
  test("Get Search deve lançar HttpStatusCodeException se o código da requisição não estiver na faixa [200,299]", getSearchDeveLancarHttpStatusCodeExceptionDadoStatusHttpMalSucedido);
  test("Get Search deve retornar itens corretamente mapeados", getSearchDeveRetornarItensCorretamenteMapeados);
}

void getSearchDeveRetornarUmaListDeResultSearchResponselModel() async
{
  var sut = GithubDatasource(HttpClientFixture());
  var result = await sut.getSearch("Qualquer coisa");

  expect(result, isNotNull);
  expect(result, isNotEmpty);
}

void getSearchDeveLancarHttpStatusCodeExceptionDadoStatusHttpMalSucedido() async {
  var sut = GithubDatasource(HttpClientThrowsExceptionFixture());

  expect(sut.getSearch("Qualquer coisa"), throwsA(isA<HttpStatusCodeException>()));
}

void getSearchDeveRetornarItensCorretamenteMapeados() async {
  var sut = GithubDatasource(HttpClientFixture());
  var result = await sut.getSearch("Qualquer coisa");
  var first = result.first;

  expect(first.title, isNotNull );
  expect(first.title.isEmpty, isFalse );
  expect(first.content, isNotNull );
  expect(first.content.isEmpty, isFalse );
  expect(first.image, isNotNull );
  expect(first.image.isEmpty, isFalse );
}

