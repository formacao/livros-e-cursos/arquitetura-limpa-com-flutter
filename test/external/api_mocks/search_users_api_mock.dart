final searchUsersMock = """
{
  "total_count": 1,
  "incomplete_results": false,
  "items": [
    {
      "login": "kennynuylla",
      "id": 29872831,
      "node_id": "MDQ6VXNlcjI5ODcyODMx",
      "avatar_url": "https://avatars3.githubusercontent.com/u/29872831?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/kennynuylla",
      "html_url": "https://github.com/kennynuylla",
      "followers_url": "https://api.github.com/users/kennynuylla/followers",
      "following_url": "https://api.github.com/users/kennynuylla/following{/other_user}",
      "gists_url": "https://api.github.com/users/kennynuylla/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/kennynuylla/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/kennynuylla/subscriptions",
      "organizations_url": "https://api.github.com/users/kennynuylla/orgs",
      "repos_url": "https://api.github.com/users/kennynuylla/repos",
      "events_url": "https://api.github.com/users/kennynuylla/events{/privacy}",
      "received_events_url": "https://api.github.com/users/kennynuylla/received_events",
      "type": "User",
      "site_admin": false,
      "score": 1.0
    }
  ]
}
""";