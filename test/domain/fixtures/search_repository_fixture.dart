import 'package:clean_arch/domain/entities/result_search.dart';
import 'package:clean_arch/domain/repositories/search_repository_interface.dart';

class SearchRepositoryFixture implements SearchRepositoryInterface{

  @override
  Future<List<ResultSearch>> search(String searchText) => Future.value(<ResultSearch> [
    ResultSearch(title: "Testando", content: "Isso é apenas um teste", image: "teste")
  ]);
}

