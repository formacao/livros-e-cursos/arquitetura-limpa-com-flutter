import 'package:clean_arch/domain/errors/invalid_search_text_exception.dart';
import 'package:clean_arch/domain/usecases/search_by_text.dart';
import 'package:clean_arch/domain/usecases/search_by_text_interface.dart';
import 'package:flutter_test/flutter_test.dart';
import '../fixtures/search_repository_fixture.dart';

void main() {
  final searchRepositoryFixture = SearchRepositoryFixture();

  final sut = SearchByText(searchRepositoryFixture);

  test("Deve retornar uma lista de ResultSearch", () => CallDeveRetornarUmaLista(sut));
  test("Deve lançar uma exceção de texto inválido dado texto de busca vazio", () => CallDeveLancarUmaExcecaoDadoTextoVazioOuNulo(sut));
}

void CallDeveRetornarUmaLista(SearchByTextInterface sut) async {
  final result = await sut.call("Cícero");
  expect(result, isNotNull);
  expect(result, isNotEmpty);
}

void CallDeveLancarUmaExcecaoDadoTextoVazioOuNulo(SearchByTextInterface sut) async {
  expect(sut.call(""), throwsA(isA<InvalidSearchTextException>()));
  expect(sut.call(null), throwsA(isA<InvalidSearchTextException>()));
}