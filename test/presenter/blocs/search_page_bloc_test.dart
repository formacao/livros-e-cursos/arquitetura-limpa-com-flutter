import 'package:clean_arch/dependency_injection/ioc_container.dart';
import 'package:clean_arch/domain/dependency_injection/ioc_container_interface.dart';
import 'package:clean_arch/dependency_injection/register_dependencies.dart';
import 'package:clean_arch/domain/entities/result_search.dart';
import 'package:clean_arch/domain/usecases/search_by_text_interface.dart';
import 'package:clean_arch/presenter/blocs/base/states/error_bloc_state.dart';
import 'package:clean_arch/presenter/blocs/base/states/success_bloc_state.dart';
import 'package:clean_arch/presenter/blocs/search_page_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import '../../external/fixtures/http_client_throws_exception_fixture.dart';
import './../../dependency_injection/register_fixture_dependencies.dart';


void main() {
  test("Call deve retornar um estado de sucesso com uma lista de resultados da pesquisa", CallDeveRetornarSuccessBlocStateDadoUmaRequisicaoBemSucedida);
  test("Deve dar erro dada uma requisição mal sucedida", CallDeveRetornarErrorBlocStateDadoUmaRequisicaoMalSucedida);
}

IoCContainerInterface getFreshContainer() {
  IoCContainerInterface iocContainer = IoCContainer();
  iocContainer
      .registerDatasources()
      .registerRepositories()
      .registerUseCases();

  return iocContainer;
}

void CallDeveRetornarSuccessBlocStateDadoUmaRequisicaoBemSucedida() async {
  var iocContainer = getFreshContainer()
      .registerFixtureTransientTypes();

  var sut = SearchPageBloc(iocContainer.getRequiredService<SearchByTextInterface>());
  var result = sut.call("Qualquer coisa");
  var first = await result.first;
  var castedResult = first as SuccessBlocState<List<ResultSearch>>;

  expect(castedResult.result, isNotNull);
  expect(castedResult.result, isNotEmpty);
}

void CallDeveRetornarErrorBlocStateDadoUmaRequisicaoMalSucedida() {
  var iocContainer = getFreshContainer()
      .addTransient<Client>((iocContainer) => HttpClientThrowsExceptionFixture());

  var sut = SearchPageBloc(iocContainer.getRequiredService<SearchByTextInterface>());
  var result = sut.call("Qualquer coisa");

  expect(result, emits(isA<ErrorBlocState>()));
}
