import 'package:clean_arch/domain/dependency_injection/ioc_container_interface.dart';
import 'package:http/http.dart';

import '../external/fixtures/http_client_fixture.dart';

extension FixtureRegisterDependencies on IoCContainerInterface {
  IoCContainerInterface registerFixtureTransientTypes() {
    this.addTransient<Client>((iocContainer) => HttpClientFixture());

    return this;
  }
}