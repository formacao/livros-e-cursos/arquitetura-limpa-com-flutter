import 'package:clean_arch/dependency_injection/ioc_container.dart';
import 'package:clean_arch/dependency_injection/register_dependencies.dart';
import 'package:clean_arch/domain/dependency_injection/ioc_container_interface.dart';
import 'package:clean_arch/domain/usecases/search_by_text_interface.dart';
import 'package:flutter_test/flutter_test.dart';

import './../dependency_injection/register_fixture_dependencies.dart';

void main() {
  IoCContainerInterface sut = IoCContainer();
  sut
      .registerFixtureTransientTypes()
      .registerDatasources()
      .registerRepositories()
      .registerUseCases();

  test("Deve recuperar os casos de uso", () => DeveRecuperarUseCases(sut));
  test("SearchByTextUseCase deve retornar uma lista", () => SearchByTextUseCaseDeveRetornarListaNaoVazia(sut));
}

void DeveRecuperarUseCases(IoCContainerInterface sut) {
  var injectedInstance = sut.getRequiredService<SearchByTextInterface>();
  expect(injectedInstance, isNotNull);
}

void SearchByTextUseCaseDeveRetornarListaNaoVazia(IoCContainerInterface sut) async {
  var useCase = sut.getRequiredService<SearchByTextInterface>();
  var result = await useCase.call("Qualquer coisa");

  expect(result, isNotNull);
  expect(result, isNotEmpty);
}