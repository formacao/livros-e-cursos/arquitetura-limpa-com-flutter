import 'package:clean_arch/infrastructure/datasources/search_datasource_interface.dart';
import 'package:clean_arch/infrastructure/response_models/result_search_response_model.dart';

class SearchDatasourceFixture implements SearchDataSourceInterface
{
  @override
  Future<List<ResultSearchResponseModel>> getSearch(String searchText) {

    var result = <ResultSearchResponseModel> [
      ResultSearchResponseModel(title: "Teste", content: "Teste", image: "Teste")
    ];

    return Future.value(result);
  }
  
}