import 'package:clean_arch/infrastructure/datasources/search_datasource_interface.dart';
import 'package:clean_arch/infrastructure/response_models/result_search_response_model.dart';

class SearchDatasourceThrowsErrorFixture implements SearchDataSourceInterface
{
  @override
  Future<List<ResultSearchResponseModel>> getSearch(String searchText) {
    throw UnimplementedError();
  }

}