import 'package:clean_arch/domain/errors/datasource_failure_error.dart';
import 'package:clean_arch/domain/repositories/search_repository_interface.dart';
import 'package:clean_arch/infrastructure/datasources/search_datasource_interface.dart';
import 'package:clean_arch/infrastructure/repositories/search_repository.dart';
import 'package:flutter_test/flutter_test.dart';

import '../fixtures/search_datasorce_throws_error_fixture.dart';
import '../fixtures/search_datasource_fixture.dart';

void main()
{
  test("Search deve retornar uma lista de ResultSearch", SearchDeveRetornarListaNaoVazia);
  test("Search deve lançar SearchFailureError quando o datasource lançar algum erro",  SearchDeveRetornarDatasourceFailureErrorQuandoDatasourceLancarExcecao);
}

void SearchDeveRetornarListaNaoVazia() async {
  final SearchDataSourceInterface datasourceFixture = SearchDatasourceFixture();
  final SearchRepository sut = SearchRepository(datasourceFixture);
  final result = await sut.search("Qualquer Coisa");

  expect(result, isNotNull);
  expect(result, isNotEmpty);
}

void SearchDeveRetornarDatasourceFailureErrorQuandoDatasourceLancarExcecao(){
  final SearchDataSourceInterface datasourceFixture = SearchDatasourceThrowsErrorFixture();
  final SearchRepository sut = SearchRepository(datasourceFixture);

  expect(sut.search("Qualquer Coisa"), throwsA(isA<DatasourceFailureError>()));
}
