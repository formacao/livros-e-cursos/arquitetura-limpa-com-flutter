import 'package:clean_arch/presenter/blocs/base/states/bloc_state_base.dart';

class ErrorBlocState implements BlocStateBase {
  final _message;
  ErrorBlocState(this._message);

  String get errorMessage => _message;

  @override
  bool get hasErrors =>true;


}