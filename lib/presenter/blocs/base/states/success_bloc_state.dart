import 'package:clean_arch/presenter/blocs/base/states/bloc_state_base.dart';

class SuccessBlocState<TResult> extends BlocStateBase {
  final TResult _result;
  SuccessBlocState(this._result);

  String get errorMessage => ""; //TODO: Create an Error for this case

  @override
  bool get hasErrors => false;

  TResult get result => _result;

}