import 'package:clean_arch/presenter/blocs/base/states/bloc_state_base.dart';

abstract class BlocBase<TInput> {

  Stream<BlocStateBase> call (TInput input);
}