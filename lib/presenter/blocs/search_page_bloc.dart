import 'package:clean_arch/domain/entities/result_search.dart';
import 'package:clean_arch/domain/usecases/search_by_text_interface.dart';
import 'package:clean_arch/presenter/blocs/base/bloc_base.dart';
import 'package:clean_arch/presenter/blocs/base/states/bloc_state_base.dart';
import 'package:clean_arch/presenter/blocs/base/states/error_bloc_state.dart';
import 'package:clean_arch/presenter/blocs/base/states/success_bloc_state.dart';

class SearchPageBloc implements BlocBase<String> {

  final SearchByTextInterface _searchByTextUseCase;

  SearchPageBloc(this._searchByTextUseCase);

  @override
  Stream<BlocStateBase> call(String input) async* {
    try {
      var result = await _searchByTextUseCase.call(input);
      yield SuccessBlocState<List<ResultSearch>>(result);
    }
    catch(error) {
      yield ErrorBlocState("An error occurred");
    }
  }

}