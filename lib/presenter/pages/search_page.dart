import 'package:flutter/material.dart';

class SearchPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(
      title: Text("Github Search"),
    ),
    body: Column(
      children: [
        Padding(
          padding:  EdgeInsets.all(8),
          child: TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: "Search"
            ),
          ),
        ),
        Expanded(
          child: ListView.builder(itemBuilder: (_, id) {
            return ListTile();
          }),
        )
      ],
    ),
  );

}