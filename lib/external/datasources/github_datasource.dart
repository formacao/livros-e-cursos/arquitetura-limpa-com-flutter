import 'dart:convert';
import 'package:clean_arch/domain/errors/http_status_code_exception.dart';
import 'package:clean_arch/infrastructure/datasources/search_datasource_interface.dart';
import 'package:clean_arch/infrastructure/response_models/result_search_response_model.dart';
import 'package:http/http.dart';

class GithubDatasource implements SearchDataSourceInterface{

  final Client _httpClient;

  GithubDatasource(this._httpClient);

  @override
  Future<List<ResultSearchResponseModel>> getSearch(String searchText) async {
    var normalizedSearchText = searchText.replaceAll(" ", "+");
    var response = await _httpClient.get("https://api.github.com/search/users?q=${normalizedSearchText}");
    if(response.statusCode < 200 || response.statusCode >= 300) throw HttpStatusCodeException();

    var responseMap = json.decode(response.body);
    return (responseMap["items"] as List<dynamic>)
        .map((x) => ResultSearchResponseModel.fromMap(x))
        .toList();
  }
  
}