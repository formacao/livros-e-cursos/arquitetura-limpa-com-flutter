import 'package:clean_arch/domain/dependency_injection/ioc_container_interface.dart';
import 'package:clean_arch/domain/repositories/search_repository_interface.dart';
import 'package:clean_arch/domain/usecases/search_by_text.dart';
import 'package:clean_arch/domain/usecases/search_by_text_interface.dart';
import 'package:clean_arch/external/datasources/github_datasource.dart';
import 'package:clean_arch/infrastructure/datasources/search_datasource_interface.dart';
import 'package:clean_arch/infrastructure/repositories/search_repository.dart';
import 'package:http/http.dart';

extension RegisterDependencies on IoCContainerInterface {

  IoCContainerInterface registerTransientTypes() {
    this.addTransient((iocContainer) => Client());

    return this;
  }

  IoCContainerInterface registerDatasources() {
    this.addSingleton<SearchDataSourceInterface>((iocContainer) => GithubDatasource(iocContainer.getRequiredService<Client>()));

    return this;
  }

  IoCContainerInterface registerRepositories() {
    this.addSingleton<SearchRepositoryInterface>((iocContainer) => SearchRepository(iocContainer.getRequiredService<SearchDataSourceInterface>()));

    return this;
  }

  IoCContainerInterface registerUseCases() {
    this.addSingleton<SearchByTextInterface>((iocContainer) => SearchByText(iocContainer.getRequiredService<SearchRepositoryInterface>()));

    return this;
  }
}