import 'package:clean_arch/domain/dependency_injection/ioc_container_interface.dart';
import 'package:get_it/get_it.dart';

class IoCContainer implements IoCContainerInterface{

  final _getItInstance = GetIt.asNewInstance();

  @override
  IoCContainerInterface addSingleton<Type>(Type Function(IoCContainerInterface iocContainer) function)
  {
    _getItInstance.registerLazySingleton<Type>(() => function(this));
    return this;
  }

  @override
  IoCContainerInterface addTransient<Type>(Type Function(IoCContainerInterface iocContainer) function) {
    _getItInstance.registerFactory<Type>(() => function(this));
    return this;
  }

  @override
  Type getRequiredService<Type>() => _getItInstance.get<Type>();

}