import 'package:clean_arch/infrastructure/response_models/result_search_response_model.dart';

abstract class SearchDataSourceInterface {
  Future<List<ResultSearchResponseModel>> getSearch(String searchText);
}