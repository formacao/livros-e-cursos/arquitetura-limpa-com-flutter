import 'package:clean_arch/domain/entities/result_search.dart';
import 'package:clean_arch/domain/errors/datasource_failure_error.dart';
import 'package:clean_arch/domain/repositories/search_repository_interface.dart';
import 'package:clean_arch/infrastructure/datasources/search_datasource_interface.dart';

class SearchRepository implements SearchRepositoryInterface {
  final SearchDataSourceInterface _datasource;

  SearchRepository(this._datasource);

  @override
  Future<List<ResultSearch>> search(String searchText) async
  {
    try {
      return await _datasource.getSearch(searchText);
    }
    catch(e) {
      throw DatasourceFailureError();
    }
  }

}