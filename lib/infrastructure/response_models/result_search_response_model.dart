import 'dart:convert';
import 'package:clean_arch/domain/entities/result_search.dart';

class ResultSearchResponseModel extends ResultSearch {

  ResultSearchResponseModel({title: String, content: String, image: String}) : super(title: title, content: content, image: image);

  Map<String, dynamic> toMap() => {'title': title, 'content': content, 'image': image};

  String toJson() => json.encode(toMap());

  static ResultSearchResponseModel fromMap(Map<String, dynamic> map) {
    if(map == null) throw ArgumentError("map is null");

    var title = map["title"].toString();
    var content = map["content"].toString();
    var image = map["image"].toString();

    return ResultSearchResponseModel(title: title, content: content, image: image);
  }

  static ResultSearchResponseModel fromJson(String encodedJson)
  {
    if(encodedJson == null || encodedJson == "") throw ArgumentError("Invalid encodedJson value");

    ResultSearchResponseModel.fromMap(json.decode(encodedJson));
  }
}
