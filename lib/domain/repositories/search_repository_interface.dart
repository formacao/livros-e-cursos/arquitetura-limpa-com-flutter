import 'package:clean_arch/domain/entities/result_search.dart';

abstract class SearchRepositoryInterface {
  Future<List<ResultSearch>> search(String searchText);
}