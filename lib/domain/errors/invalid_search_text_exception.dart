import 'package:clean_arch/domain/errors/search_failure_error_interface.dart';

class InvalidSearchTextException implements SearchFailureErrorInterface {}