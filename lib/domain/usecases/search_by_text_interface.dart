import 'package:clean_arch/domain/entities/result_search.dart';

abstract class SearchByTextInterface {

  Future<List<ResultSearch>> call(String searchText);
}