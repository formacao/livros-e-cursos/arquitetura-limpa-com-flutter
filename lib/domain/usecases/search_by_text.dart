import 'package:clean_arch/domain/entities/result_search.dart';
import 'package:clean_arch/domain/errors/invalid_search_text_exception.dart';
import 'package:clean_arch/domain/repositories/search_repository_interface.dart';
import 'package:clean_arch/domain/usecases/search_by_text_interface.dart';

class SearchByText implements SearchByTextInterface{

  final SearchRepositoryInterface _repository;

  SearchByText(this._repository);

  @override
  Future<List<ResultSearch>> call(String searchText) async
  {
    if(searchText == "" || searchText == null) throw InvalidSearchTextException();
    return await _repository.search(searchText);
  }

}