abstract class IoCContainerInterface {
  IoCContainerInterface addSingleton<Type>(Type function(IoCContainerInterface iocContainer));
  IoCContainerInterface addTransient<Type>(Type function(IoCContainerInterface iocContainer));

  Type getRequiredService<Type>();
}