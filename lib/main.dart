import 'package:clean_arch/dependency_injection/ioc_container.dart';
import 'package:clean_arch/domain/dependency_injection/ioc_container_interface.dart';
import 'package:clean_arch/dependency_injection/register_dependencies.dart';
import 'file:///C:/Users/mathe/Documents/Tutoriais/Flutter/Flutterando/clean_arch/lib/presenter/pages/search_page.dart';
import 'package:flutter/material.dart';

final IoCContainerInterface iocContainer = IoCContainer();

void main() {

  iocContainer
      .registerTransientTypes()
      .registerDatasources()
      .registerRepositories()
      .registerUseCases();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'GitHub Search',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SearchPage()
    );
  }
}

